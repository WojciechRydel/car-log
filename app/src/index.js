import React from 'react'
import ReactDOM from 'react-dom'
import ListPage from './components/ListPage'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Dashboard } from './components/dashboard';
import { ApolloProvider, createNetworkInterface, ApolloClient } from 'react-apollo'
import 'tachyons'
import './index.css'

const networkInterface = createNetworkInterface({
  uri: process.env.REACT_APP_GRAPHCOOL_ENDPOINT,
})

const client = new ApolloClient({ networkInterface })

ReactDOM.render(
  <ApolloProvider client={client}>
    <Router>
      <div>
        <Route exact path='/' component={Dashboard} />
      </div>
    </Router>
  </ApolloProvider>,
  document.getElementById('root'),
)
